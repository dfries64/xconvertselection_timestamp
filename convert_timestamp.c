/* https://en.wikipedia.org/wiki/Xlib#Example
 * Modified to request the STRING and TIMESTAMP values for the primary selection
 * this is to debug and verify programs set the TIMESTAMP.
 */


 /*
  * Simple Xlib application drawing a box in a window.
  * gcc input.c -o output -lX11
  */

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

int main(void)
{
    Display *display;
    Window window;
    XEvent event;
    char msg[80] = {};
    const char *msg2 = "'?' for help";
    int s;
    Atom LA_PRIMARY, LA_TIMESTAMP, LA_STRING;

    /* open connection with the server */
    display = XOpenDisplay(NULL);
    if (display == NULL)
    {
        fprintf(stderr, "Cannot open display\n");
        exit(1);
    }

    s = DefaultScreen(display);

    /* create window */
    window = XCreateSimpleWindow(display, RootWindow(display, s), 10, 10, 200, 200, 1,
                           BlackPixel(display, s), WhitePixel(display, s));

    snprintf(msg, sizeof(msg)-1, "window 0x%08lx", window);
    printf("%s\n", msg);

    /* select kind of events we are interested in */
    XSelectInput(display, window, ExposureMask | KeyPressMask);

    /* map (show) the window */
    XMapWindow(display, window);

    LA_PRIMARY = XInternAtom(display, "PRIMARY", 1);
    LA_TIMESTAMP = XInternAtom(display, "TIMESTAMP", 1);
    LA_STRING = XInternAtom(display, "STRING", 1);
    if(LA_PRIMARY == None || LA_TIMESTAMP == None || LA_STRING == None)
    {
        printf("couldn't get needed atoms %lu %lu\n", LA_TIMESTAMP, LA_PRIMARY);
        return 1;
    }

    XFlush(display);

    /* event loop */
    for (;;)
    {
        XNextEvent(display, &event);

        /* draw or redraw the window */
        if (event.type == Expose)
        {
            XFillRectangle(display, window, DefaultGC(display, s), 20, 20, 10, 10);
            XDrawString(display, window, DefaultGC(display, s), 30, 50, msg, strlen(msg));
            XDrawString(display, window, DefaultGC(display, s), 30, 70, msg2, strlen(msg2));
        }

        if (event.type == KeyPress)
        {
            int t_pressed, p_pressed;
            KeySym sym = XKeycodeToKeysym(display, event.xkey.keycode, 0);
            const char *s = XKeysymToString(sym);
            t_pressed = !strcmp("t", s);
            p_pressed = !strcmp("p", s);
            printf("KeyPress %u %s\n", event.xkey.keycode, s);
            if(t_pressed || p_pressed)
            {
                Atom target, property;
                if(t_pressed)
                {
                    target = LA_TIMESTAMP;
                    property = LA_TIMESTAMP;
                    printf("\nRequest TIMESTAMP\n");
                }
                if(p_pressed)
                {
                    target = LA_STRING;
                    property = LA_STRING;
                    printf("\nRequest STRING\n");
                }
                Window owner = XGetSelectionOwner(display, LA_PRIMARY);
                printf("owner 0x%08lx\n", owner);
                if(owner != None)
                {
                    // selection: which selection or clipboard to operate on
                    // target: what format to return the data in
                    // property: the property the owner is supposed to put
                    // the answer into on window
                    // window: what property the answer is to be put in
                    XConvertSelection(display, LA_PRIMARY, target,
                        property, window, CurrentTime);
                }
            }
            else if(!strcmp("d", s))
            {
                printf("deleting properties\n");
                XDeleteProperty(display, window, LA_TIMESTAMP);
                XDeleteProperty(display, window, LA_STRING);
            }
            else if(!strcmp("q", s))
	    {
                printf("exiting\n");
	    	break;
	    }
            else if(!strcmp("Shift_L", s) || !strcmp("Shift_R", s))
            {
                // ignore to get ?
            }
            else if(!strcmp("?", s) || !strcmp("slash", s)) // ?
            {
                printf("keys\np\tget primary selection (requesting string)\n"
                    "t\tget timestamp for primary selection\n"
                    "d\tdelete target properties on crated window\n"
                    "q\tquit\n"
                    "?\thelp\n");
            }
        }

        if(event.type == SelectionNotify)
        {
            printf("received SelectionNotify event\n");
            if(event.xselection.property == None)
            {
                printf("failed property is None\n");
            }
            else
            {
                XTextProperty prop;
                if(XGetTextProperty(display, event.xselection.requestor,
                    &prop, event.xselection.property)  == 0)
                {
                    printf("failed to read property\n");
                }
                else
                {
                    printf("have property format %u\n", prop.format);
                    if(prop.format == 8)
                    {
                        // assume it is a string
                        printf("'%s'\n", prop.value);
                    }
                    else if(prop.format == 32)
                    {
                        // assume 32 bit integer
                        printf("0x%08x, %u\n",
                            *(uint32_t*)prop.value,
                            *(uint32_t*)prop.value);
                    }
                }
                XFree(prop.value);
            }
        }
    }

    /* close connection to server */
    XCloseDisplay(display);

    return 0;
}
